
/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.entgra.service;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.ws.rs.core.Response;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import org.apache.log4j.Logger;

/**
 * Implements the is valid method to validate the xml payload against a xsd file
 * and parse method to parse the the xml payload into JSON format
 */
public class UserService {

    private static Logger log = Logger.getLogger(UserService.class.getName());

    /**
     *
     * @param xmlData is the xml payload
     * @return http status code and the xml payload
     */
    public boolean isValid(String xmlData){

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            //File schemaFile = new File("/home/achala/Entgra/Personal/maven_webapp/src/resources/user.xsd");
            //Schema schema = schemaFactory.newSchema(schemaFile);
            Document document = documentBuilder.parse(new InputSource(new StringReader(xmlData)));
            String path = document.getElementsByTagName("context").item(0).getTextContent();

            Schema schema = schemaFactory.newSchema(new File(path));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new StringReader(xmlData)));
            return true;

        } catch (SAXException | IOException | ParserConfigurationException e) {
            log.error(e);
            return false;
        }

    }

    /**
     *
     * @param xmlPayload xml payload
     * @return JSON formatted string
     */
    public Response parse(String xmlPayload) {
        try {

            JSONObject xmlJSONObj = XML.toJSONObject(xmlPayload);
            String jsonPrettyPrintString = xmlJSONObj.toString();
            System.out.println(jsonPrettyPrintString);
            return Response.ok().entity(jsonPrettyPrintString).build();

        } catch (JSONException je) {
            System.out.println(je.toString());

            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(xmlPayload).build();
        }
    }

}
