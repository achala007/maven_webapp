/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.entgra.dao;

import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Implementation of the user dao class to persist the bean data
 */
public class UserDAOImpl implements UserDAO {

    static Logger log = Logger.getLogger(UserDAOImpl.class.getName());

    static DAOManager jdbcObj = new DAOManager();
    static Connection connObj = null;
    static PreparedStatement pstmtObj = null;
    static DataSource dataSource;

    static {
        try {
            dataSource = jdbcObj.setUpPool();
        } catch (Exception e) {
            log.error(e);
        }
    }

    /**
     *
     * @param entity is the user instance that needs to be persist to the db
     */
    public void persist(UserBean entity) {



        String sqlQuery = "INSERT INTO User.user (id ,name, gender, age) VALUES (?,?,?,?)";

        try {

            jdbcObj.printDbStatus();
            connObj = dataSource.getConnection();
            jdbcObj.printDbStatus();

            pstmtObj = connObj.prepareStatement(sqlQuery);
            pstmtObj.setString(1, entity.getId());
            pstmtObj.setString(2, entity.getName());
            pstmtObj.setString(3, entity.getGender());
            pstmtObj.setString(4, String.valueOf(entity.getAge()));
            pstmtObj.executeUpdate();

        } catch (Exception sqlException) {
            log.error(sqlException);

        } finally {
            try {
                if (pstmtObj != null) {
                    pstmtObj.close();
                }
                if (connObj != null) {
                    connObj.close();
                }
            } catch (Exception sqlException) {
                log.error(sqlException);
            }
        }
        jdbcObj.printDbStatus();

    }

}
