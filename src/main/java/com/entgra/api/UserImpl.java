
/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.entgra.api;

import com.entgra.dao.UserBean;
import com.entgra.dao.UserDAOImpl;
import com.entgra.service.UserService;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

/**
 * This class will implement the validateUserDetails(), parseToJSON() and addToDb() methods
 */
@Path("/user")
public class UserImpl implements User {

    private UserService service = new UserService();
    private UserDAOImpl userDAO;
    private final static Logger log = Logger.getLogger(UserImpl.class);

    public UserImpl() {
        userDAO = new UserDAOImpl();
    }

    @Override
    public Response validateUserDetails(String xmlPayload) {

        boolean valid = service.isValid(xmlPayload);

        if (valid) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(xmlPayload).build();
        }

    }

    @Override
    public Response parseToJSON(String xmlPayload) {

        boolean valid = service.isValid(xmlPayload);

        if (valid) {
            return service.parse(xmlPayload);

        } else {
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(xmlPayload).build();
        }
    }

    @Override
    public Response addToDB(String xmlPayload) {

        boolean valid = service.isValid(xmlPayload);
        DocumentBuilder builder;
        Document doc = null;

        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            log.error(e);

            return Response.status(400).entity(xmlPayload).build();
        }

        if (valid) {

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(xmlPayload));

            try {
                doc = builder.parse(src);
            } catch (SAXException | IOException e) {
                log.error(e);

            }

            assert doc != null;
            String userId = doc.getElementsByTagName("id").item(0).getTextContent();
            String userName = doc.getElementsByTagName("name").item(0).getTextContent();
            String gender = doc.getElementsByTagName("gender").item(0).getTextContent();
            int age = Integer.parseInt(doc.getElementsByTagName("age").item(0).getTextContent());

            UserBean userBean = new UserBean(userId, userName, gender, age);
            log.debug("userBean"+ userBean);
            userDAO.persist(userBean);
            return Response.status(Response.Status.OK).entity(userBean).build();

        } else {
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(xmlPayload).build();
        }
    }


}
