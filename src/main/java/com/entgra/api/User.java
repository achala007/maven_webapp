
/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.entgra.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface User {

        /**
         *
         * @param xmlData is the xml payload that needs to be validated
         * @return the http status code after the operation
         */
        @Path("/validate")
        @POST
        @Consumes(MediaType.APPLICATION_XML)
        Response validateUserDetails (String xmlData);

        /**
         *
         * @param xmlData is the xml payload that needs to be converted to JSON
         * @return the JSON formatted response
         */
        @Path("/parse")
        @POST
        @Consumes(MediaType.APPLICATION_XML)
        @Produces(MediaType.APPLICATION_JSON)
        Response parseToJSON (String xmlData);

        /**
         *
         * @param xmlData is the xml payload that needs to be inserted into the db
         * @return the http status code on the
         */
        @Path("/add")
        @POST
        @Consumes(MediaType.APPLICATION_XML)
        @Produces(MediaType.APPLICATION_JSON)
        Response addToDB (String xmlData);
}
